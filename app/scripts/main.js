(function () {
  "use strict";

  var CONFIG = {
    cellHeight: 20,
    cellWidth: 20,
    gridWidth: 40,
    gridHeight: 40,
    speed: 100
  };

  var DIRECTIONS = {
    left: 37,
    up: 38,
    right: 39,
    down: 40
  };

  var grid, gridContainer, snake, gameIntervalId, game, gameState;


  gameState = {
    foodPos: [],
    score: 0
  };

  game = {
    generateFood: function genereteFood(snakeLocations) {
      var foodLoc = [];
      var invalid = true;

      while(invalid) {
        foodLoc[0] = Math.floor(Math.random() * 40);
        foodLoc[1] = Math.floor(Math.random() * 40);

        invalid = this.hasPosition(snakeLocations, foodLoc);
      }

      gameState.foodPos[0] = foodLoc[0];
      gameState.foodPos[1] = foodLoc[1];
    },

    hasPosition: function (posSet, position) {
      var invalid = false;
      for (var i = 0, len = posSet.length; i < len; i++) {
        var loc = posSet[i];
        if (position[0] == loc[0] && position[1] == loc[1]) {
          invalid = true;
          break;
        }
      }
      return invalid;
    },

    increaseScore: function () {
      gameState.score += 1;
    }
  };

  var Snake = function (initialPos, initialDir) {
    this.position = initialPos;
    this.direction = initialDir;
    this.newDirection = initialDir;
    this.coords = [[19, 19], [18, 18]];
    this.coords.unshift([initialPos[0], initialPos[1]]);
  };

  Snake.prototype.move = function (gameState) {

    if ((this.direction - this.newDirection) % 2 != 0) {
      this.direction = this.newDirection;
    }

    var newPosition = this.position;

    switch (this.direction) {
      case DIRECTIONS.up:
        newPosition[1] = this.position[1] - 1;
        break;
      case DIRECTIONS.down:
        newPosition[1] = this.position[1] + 1;
        break;
      case DIRECTIONS.right:
        newPosition[0] = this.position[0] + 1;
        break;
      case DIRECTIONS.left:
        newPosition[0] = this.position[0] - 1;
        break;
    }

    if (newPosition[0] > CONFIG.gridWidth - 1 ||
    newPosition[0] < 0 ||
    newPosition[1] > CONFIG.gridHeight - 1 ||
    newPosition[1] < 0 ||
    game.hasPosition(this.coords, newPosition)) {
      return false;
    } else {
      this.position = newPosition;
      if (gameState.foodPos[0] == this.position[0] && gameState.foodPos[1] == this.position[1]) {
        game.generateFood(this.coords);
        game.increaseScore();
      } else {
        this.coords.pop();
      }
      this.coords.unshift([this.position[0], this.position[1]]);
      return true;
    }

  };

  var init = function () {
    grid = Array.matrix(CONFIG.gridWidth, CONFIG.gridHeight, " ");
    gridContainer = $('#gridContainer');
    snake = new Snake([20, 20], DIRECTIONS.right);
    game.generateFood(snake.coords);
    generate(grid);

    $('body').keydown(function (e) {
      var key = e.which;
      if (key >= DIRECTIONS.left && key <= DIRECTIONS.down) {
        snake.newDirection = key;
      }
    });

    gameIntervalId = window.setInterval(function () {
      if(!snake.move(gameState)) {
        clearInterval(gameIntervalId);
        if (confirm("Game Over! Restart?")) {
          init();
        }
      }

      $('.snake').removeClass('snake');
      snake.coords.forEach(function (pos) {
        $('#' + getIdStr(pos[0], pos[1])).addClass("snake");
      });

      var foodPos = gameState.foodPos;
      $('.food').removeClass('food');
      $('#' + getIdStr(foodPos[0], foodPos[1])).addClass("food");

      $('#gameScore').text(gameState.score);
    }, CONFIG.speed);
  };

  var getIdStr = function (i, j) {
    return i + '-' + j;
  };

  var generate = function (grid) {
    var xPos = 0, yPos = 0;

    for (var i = 0; i < grid.length; i++) {
      var rowLength = grid[i].length;
      for (var j = 0; j < rowLength; j++) {

        $( "<div></div>" )
          .attr('id', getIdStr(i, j))
          .addClass( "gridCell" )
          .offset({
            top: yPos,
            left: xPos
          })
          .appendTo(gridContainer);

        yPos += CONFIG.cellHeight;
      }
      xPos += CONFIG.cellWidth;
      yPos = 0;
    }
  };

  window.onload = init;

})();

